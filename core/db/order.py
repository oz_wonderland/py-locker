from datetime import datetime

from sqlalchemy import Column, select, func
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.sql.sqltypes import Integer, String

from core.db.db import Base, db_session


class Order(Base):
    __tablename__ = 'orders'

    id = Column(Integer, primary_key=True)
    code = Column(String(6))
    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship("User", back_populates="orders")
    status = Column(Integer)
    door = Column(String)
    created_at = Column(String)
    last_updated_at = Column(String)

    def __init__(self, code, user_id):
        utc = datetime.utcnow()
        self.code = code
        self.user_id = user_id
        self.created_at = utc
        self.last_updated_at = utc

    def __repr__(self):
        return f"<order({self.id!r}, {self.code!r})"

    def create(code, user_id):
        new_order = Order(code, user_id)
        with db_session() as session:
            session.begin()
            session.add(new_order)
            session.commit()

    def getByCode(code):
        with db_session() as session:
            session.begin()
            session.query(Order).filter(Order.code == func.upper(code)).first()
