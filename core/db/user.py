from sqlalchemy import Column, func
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import Integer, String

from core.db.db import Base, db_session


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    lastname = Column(String)
    firstname = Column(String)
    email = Column(String)
    phone = Column(String)
    code = Column(String(6))
    sex = Column(Integer)
    type = Column(Integer)
    status = Column(Integer)
    orders = relationship("Order")
    created_at = Column(Integer)
    last_updated_at = Column(Integer)

    def __init__(self, firstname, lastname):
        self.firstname = firstname
        self.lastname = lastname

    def __repr__(self):
        return f"<user({self.lastname!r}, {self.firstname!r})"

    def getByCode(code):
        with db_session() as session:
            session.begin()
            return session.query(User).filter(
                User.code == func.lower(code)).first()

    def getByType(type):
        with db_session() as session:
            session.begin()
            return session.query(User).filter(User.type == type).all()
