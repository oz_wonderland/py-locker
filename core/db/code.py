from sqlalchemy import Column
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import Integer, String
from db import Base


class Code(Base):
    __tablename__ = 'codes'

    id = Column(Integer, primary_key=True)
    value = Column(String)
    user_id = relationship("User")
    created_by = relationship("User")
    created_at = Column(String)
    last_updated_by = relationship("User")
    last_updated_at = Column(String)

    def __repr__(self):
        return f"<Code({self.id!r}, {self.value!r})"

    def getByUserID(user_id):
        return "toto"
