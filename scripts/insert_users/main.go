package main

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"go.uber.org/zap"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func main() {
	logger, err := zap.NewDevelopment()
	if err != nil {
		panic(err)
	}
	defer logger.Sync()
	sugar := logger.Sugar()
	sugar.Info("connecting to xlocker db...")

	db, err := gorm.Open(sqlite.Open("sqlite.db"), &gorm.Config{})
	if err != nil {
		sugar.With("err", err.Error()).Error("failed to connect to db")
		return
	}
	db.AutoMigrate(&User{})
	sugar.Info("connected to xlocker db")
	users, err := readUsersFile()
	if err != nil {
		sugar.Errorf("failed to read users.json file %+v", err)
		os.Exit(-1)
	}
	sugar.Infof("found %d users", len(users))
	res := db.Create(&users)
	if res.Error != nil {
		sugar.Errorf("failed to bulk insert users %+v", res.Error)
		os.Exit(-1)
	}
	sugar.Infof("successfully inserted %d test users", res.RowsAffected)
	sugar.Info("script is done. Shuting down now")
}

func readUsersFile() (Users, error) {
	data, err := ioutil.ReadFile("./users.json")
	if err != nil {
		return nil, err
	}
	users := &Users{}
	if err = json.Unmarshal(data, users); err != nil {
		return nil, err
	}
	return *users, nil
}
