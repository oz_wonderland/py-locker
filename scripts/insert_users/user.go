package main

type Status byte

type Type byte

type Sex byte

type Users []User

const (
	UNKNOW_TYPE Type = iota
	ADMIN
	PARTNER
	CLIENT
)

const (
	UNKNOW_SEX Sex = iota
	WOMAN
	MAN
	OTHER
)

const (
	UNKNOW_STATUS Status = iota
	INACTIVE
	ACTIVE
	DELETED
	SUSPECT
)

type User struct {
	ID            int    `json:"id,omitempty" gorm:"primaryKey"`
	Type          Type   `json:"type,omitempty"`
	Sex           Sex    `json:"sex,omitempty"`
	Status        Status `json:"status,omitempty"`
	Code          string `json:"code,omitempty" gorm:"index;size:6"`
	Lastname      string `json:"lastname,omitempty"`
	Firstname     string `json:"firstname,omitempty"`
	Email         string `json:"email,omitempty"`
	Phone         string `json:"phone,omitempty"`
	CreatedAt     int64  `json:"created_at,omitempty" gorm:"autoCreateTime"`
	LastUpdatedAt int64  `json:"last_updated_at,omitempty" gorm:"autoUpdateTime"`
}
