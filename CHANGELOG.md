## 0.3.0 (October 11, 2021)

*_Features_*
  - add locker management interface
  - add order creation
  - update the way we use sqlalchemy session
  - add popup when unknow code is inserted
  - simplified keyboard code
  - deactivate/remove unused characters from keyboard
  - add custom style for keyboard
  - add custom layout for keyboard

*_Fixes_*
  - Removed 0 and O from RandomCode 

## 0.0.2 (July 12, 2021)

*_Features_*
  - qr code scanner working again
  - cleanup qml folder
  - switch db to single sqlite file
  - update user creation script
  - add rule to format qml in makefile
  - add new top bar for all screens
  - users displayed on management page are loaded from `db`
  - remove unused views

## 0.0.1 (April 11, 2021)

*_Features_*
  - switch to py-qt 
