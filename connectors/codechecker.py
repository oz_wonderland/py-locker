from PySide2.QtCore import Slot, QObject
from PySide2.QtCore import Signal, Slot, QObject

from core.db.user import User
from core.db.order import Order


class Backend(QObject):
    openSettingsView = Signal()
    openErrorPopup = Signal()

    def __init__(self):
        super().__init__()

    def openSettings(self):
        self.openSettingsView.emit()

    def openError(self):
        self.openErrorPopup.emit()


backend = Backend()


class CodeCheckerConnector(QObject):
    def __init__(self):
        QObject.__init__(self)

    @Slot(str, result=str)
    def codeverify(self, code):
        user = User.getByCode(code)

        print(f"received code {code}")
        # first try to find a matching user
        if user is not None:
            print(f"found user {user.id} with code {code}")
            backend.openSettings()
            return

        print("user not found looking for an order")
        # then try to find a matching order
        order = Order.getByCode(code)
        if order is not None:
            print(f"found one order {order}")
            return

        backend.openError()
