from PySide2.QtCore import Qt, QAbstractListModel, QModelIndex, Slot

from core.db.user import User


class UserConnector(QAbstractListModel):
    # Atributes defined as roles
    users = []
    firstname, lastname, phone, type = range(
        Qt.UserRole + 1, Qt.UserRole + 5)

    def __init__(self, parent=None):
        super(UserConnector, self).__init__(parent)
        self.users = []

    def data(self, index, role=Qt.DisplayRole):
        if index.isValid():
            row = index.row()
            if role in self.roleNames().keys():
                key = self.roleNames()[role]
                return self.users[row][key.decode()]

    def rowCount(self, _=QModelIndex()):
        return len(self.users)

    def roleNames(self):
        return {UserConnector.firstname: b'firstname', UserConnector.lastname: b'lastname', UserConnector.phone: b'phone', UserConnector.type: b'type'}

    @Slot(int)
    def getUsersByType(self, type):
        self.beginResetModel()
        self.users = []
        print(f"looking for users of type {type}")
        users = User.getByType(type)

        print(f"found {len(users)} users with type {type}")
        for user in users:
            userType = 'admin' if user.type == 1 else 'asv'
            self.users.append({'firstname': '' + user.firstname + '', 'lastname': '' + user.lastname +
                               '', 'phone': '' + user.phone + '', 'type': '' + userType + ''})
        self.endResetModel()
