import os
import sys

# Py Qt imports
from PySide2.QtWidgets import QApplication
from PySide2.QtQml import QQmlApplicationEngine, qmlRegisterType
from PySide2.QtCore import QUrl


# own imports
from core.db.db import init_db
from camera import QrCodeReader
from randomcode import RandomCode
from connectors.user import UserConnector
from connectors.codechecker import CodeCheckerConnector, backend
import resources

os.environ["QT_IM_MODULE"] = "qtvirtualkeyboard"
os.environ["QT_VIRTUALKEYBOARD_LAYOUT_PATH"] = "qml/VirtualKeyboard/layouts/"
os.environ["QML2_IMPORT_PATH"] = "qml/VirtualKeyboard/"
os.environ["QT_VIRTUALKEYBOARD_STYLE"] = "vet_and_go"


def run():
    app = QApplication([])
    qmlRegisterType(QrCodeReader, "QrCodeReader",
                    1, 0, "QrCodeReader")
    codeCheckerConnector = CodeCheckerConnector()
    userConnector = UserConnector()
    randomcode = RandomCode()

    engine = QQmlApplicationEngine()
    engine.load(QUrl('qrc:/qml/main.qml'))
    engine.rootContext().setContextProperty("randomcode", randomcode)
    engine.rootContext().setContextProperty(
        "codeCheckerConnector", codeCheckerConnector)
    engine.rootContext().setContextProperty("userConnector", userConnector)
    if not engine.rootObjects():
        return -1
    engine.rootObjects()[0].setProperty('backend', backend)
    return app.exec_()


if __name__ == "__main__":
    init_db()
    sys.exit(run())
