import random
import string

from PySide2.QtCore import QObject, Slot

from core.db.order import Order


class RandomCode(QObject):

    @Slot(result=str)
    def get_random_code(self):
        letters = string.ascii_uppercase + string.digits
        letters = letters.replace("O","")
        letters = letters.replace("0","")
        result_str = ''.join(random.choice(letters) for i in range(6))

        Order.create(result_str, 31)
        return result_str
