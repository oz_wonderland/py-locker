import numpy as np
from PySide2.QtMultimedia import QAbstractVideoFilter, QVideoFilterRunnable, QVideoFrame
from PySide2.QtGui import QImage
from pyzbar import pyzbar


def QImageToNp(image: QImage):
    '''  Converts a QImage into an np array '''

    incomingImage = image.convertToFormat(QImage.Format.Format_RGB32)
    width = incomingImage.width()
    height = incomingImage.height()
    ptr = incomingImage.constBits()
    arr = np.array(ptr).reshape(height, width, 4)  # Copies the data
    return arr


class QrCodeReaderRunnable(QVideoFilterRunnable):
    ''' Scan every image and tries to decode barcodes '''

    def run(self, input: QVideoFrame, *_):
        arr = QImageToNp(input.image())
        barcodes = pyzbar.decode(arr)
        for barcode in barcodes:
            barcodeData = barcode.data.decode("utf-8")
            barcodeType = barcode.type
            text = "{} ({})".format(barcodeData, barcodeType)
            print(text)
        return input


class QrCodeReader(QAbstractVideoFilter):
    def createFilterRunnable(self):
        return QrCodeReaderRunnable(self)
