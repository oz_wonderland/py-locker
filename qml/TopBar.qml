import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11

// The topbar is displayed on all screens at the top
// it contains a title (required)
// an icon (optional)
// and a back button (optional)
Rectangle {
    id: top_bar_rectangle
    property bool withLogo
    property bool withIcon
    property string iconCirclePath
    property string title

    radius: 20
    clip: false
    color: Styles.darkGreen
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.top: parent.top
    anchors.leftMargin: 23
    anchors.rightMargin: 23
    anchors.topMargin: 35

    Text {
        id: top_bar_text
        text: title
        font.weight: Font.Bold
        font.pointSize: 48
        font.letterSpacing: 1.1
        color: "white"
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: top_bar_circle.right
        anchors.leftMargin: 15
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: withIcon ? Text.AlignVCenter : 0
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Image {
        id: logo_image_bar
        visible: withLogo ? true : false
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        source: "/drawables/vetgoLogo.png"
        anchors.rightMargin: 15
    }

    Rectangle {
        id: top_bar_circle
        visible: withIcon ? true : false
        width: 60
        height: 60
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: 25
        radius: width * 0.5
        color: "#6FB98F"

        Image {
            id: top_bar_circle_image
            anchors.verticalCenter: parent.verticalCenter
            source: iconCirclePath
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}
