import QtQuick 2.15
import QtQuick.Controls 2.15

Page {
    id: user_management_view_page
    property int userType

    //Background
    Rectangle {
        color: Styles.backgroundColor
        anchors.fill: parent
    }

    TopBar {
        id: user_management_view_banner
        withIcon: true
        iconCirclePath: "/drawables/2_gears.svg"
        height: Styles.topbarHeight
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 30
        anchors.topMargin: 30
        anchors.rightMargin: 30
        title: "Gestion des profils"

        BackButton {
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.rightMargin: 15
        }
    }
    UserManagementContent {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: user_management_view_banner.bottom
        userType: user_management_view_page.userType
    }
}
