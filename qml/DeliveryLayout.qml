import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.15

//import io.qt.xlocker.codegenerator 1.0
Page {
    property int currentIndexOut

    id: delivery_view_page

    // background -----------------
    Rectangle {
        anchors.fill: parent
        color: Styles.backgroundColor
    }

    //-----------------------------
    Locker {
        id: locker2x4
        height: 560
        numberOfColums: 2 //number of columns on the locker
        onCurrentIndexChanged: {
            menssage.text = "Veuillez prendre note du
code associé à cette
commande puis cliquer sur
“Ouvrir Casier”
"
            currentIndexOut = currentIndex
            message_of_door_selected.visible = true
            message_of_door_selected.text = "Casier " + currentIndex + " choisi"
            click_door_image.visible = false
            rectangle_code.visible = true
            opendoor_button.visible = true
            code_client_label.visible = true
        }
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.verticalCenterOffset: 0
        anchors.leftMargin: 21
    }

    TopBar {
        id: top_bar
        anchors.left: locker2x4.right
        iconCirclePath: "/drawables/info_icon.svg"
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.leftMargin: 32
        anchors.rightMargin: 23
        anchors.topMargin: 42
    }

    //Sub Top Bar -----------------------
    Rectangle {
        id: sub_top_bar
        radius: 20
        anchors.left: locker2x4.right
        anchors.right: parent.right
        anchors.top: top_bar.bottom
        anchors.bottom: rectangle_message.top
        anchors.leftMargin: 250
        anchors.rightMargin: 24
        anchors.bottomMargin: 20
        anchors.topMargin: 30
        color: "#3E9F99"

        Text {
            id: text_sub_top_bar
            color: Styles.white
            text: "Préparation de livraison"
            font.pointSize: 30
            anchors.verticalCenter: parent.verticalCenter
            font.bold: true
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }

    //-----------------------------------
    Rectangle {
        id: rectangle_message
        color: Styles.white
        opacity: 0.8
        radius: 10
        anchors.left: locker2x4.right
        anchors.right: parent.right
        anchors.top: top_bar.bottom
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 37
        anchors.leftMargin: 250
        anchors.topMargin: 125
        anchors.rightMargin: 25

        Image {
            id: click_door_image
            anchors.top: parent.top
            source: "/drawables/clicked_door.png"
            anchors.horizontalCenterOffset: 18
            anchors.topMargin: 74
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Text {
            id: message_of_door_selected
            font.family: "Arial"
            font.pixelSize: 36
            horizontalAlignment: Text.AlignHCenter
            anchors.horizontalCenter: parent.horizontalCenter
            font.bold: true
            anchors.bottomMargin: 42
            visible: false
            anchors.bottom: menssage.top
        }

        Text {
            id: menssage
            font.family: "Arial"
            font.pixelSize: 24
            horizontalAlignment: Text.AlignHCenter
            anchors.topMargin: 15
            font.underline: false
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Veuillez choisir
un casier disponible
en cliquant dessus"
            anchors.verticalCenter: parent.verticalCenter
            anchors.top: click_door_image.bottom
            font.bold: true
        }
        //---------------------------------
        //Code Representation -------------
        //---------------------------------
        Label {
            id: code_client_label
            visible: false
            anchors.bottom: rectangle_code.top
            font.bold: true
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: 0
            font.pointSize: 24
            text: qsTr("Code client")
        }

        Rectangle {
            visible: false
            id: rectangle_code
            radius: 10
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.rightMargin: 50
            anchors.leftMargin: 50
            anchors.bottomMargin: 17
            height: 78
            color: Styles.backgroundColor
            Label {
                id: code_label
                color: "white"
                text: randomcode.get_random_code()
                anchors.verticalCenter: parent.verticalCenter
                font.family: Styles.codeFont.name
                font.bold: true
                font.pointSize: 60
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
        //+---------------------------------

        // Clossing Door Message-------------
        Image {
            visible: false
            anchors.left: parent.left
            id: closingDoor_image_text
            width: 64
            height: 62
            anchors.top: menssage.bottom
            source: "/drawables/closing_door.png"
            transformOrigin: Item.Right
            anchors.leftMargin: 115
            anchors.topMargin: 10
            fillMode: Image.PreserveAspectFit

            Text {
                id: closingDoor_text
                text: qsTr("Fermez la porte \npour terminer")
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.right
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
                font.pointSize: 24
                anchors.leftMargin: 21
            }
        }
        //+----------------------------------
    }

    BackButton {
        id: exit_button
        anchors.left: locker2x4.right
        anchors.bottom: parent.bottom
        anchors.leftMargin: 31
        anchors.bottomMargin: 67
    }

    //Open Door Button ----------
    IconButton {
        id: opendoor_button
        visible: false
        anchors.left: locker2x4.right
        anchors.top: top_bar.bottom
        anchors.leftMargin: 31
        anchors.topMargin: 30
        buttonText: "Ouvrir\nCasier"
        width_button: 170
        height_button: 100
        onClicked: {
            message_of_door_selected.text = "Casier " + currentIndexOut + " ouverte"
            menssage.visible = false
            closingDoor_image_text.visible = true
            locker2x4.enabled = false
            exit_button.visible = false
            cancel_operation_button.visible = true
            opendoor_button.visible = false
            function onOpen() {
                locker_repeater.itemAt(currentIndex).stateButton = 'open'
            }
        }
    }
    //+--------------------------

    //Cancel Operation Button ----------
    IconButton {
        id: cancel_operation_button
        visible: false
        anchors.left: locker2x4.right
        anchors.bottom: parent.bottom
        anchors.leftMargin: 31
        anchors.bottomMargin: 37
        buttonText: "Annuler\nOpération"
        width_button: 170
        height_button: 100
        onClicked: {
            pop_up_message_button.open()
        }
    }
    //+--------------------------

    //---------------------------PopUp's
    PopUpMessage {
        id: pop_up_message_button
        message: "Êtes-vous sûr de vouloir annuler l'opération?"
        button_text_left_popup: "NON"
        button_text_rigth_popup: "OUI"
        function onClickedYes() {
            pop_up_message_button.close()
            pop_up_close_door.open()
            timer.start()
        }
    }

    PopUpIcon {
        id: pop_up_close_door
        message: "Veuillez fermer la porte " + currentIndexOut + "."
    }

    //-----------------------------
    Timer {
        id: timer
        interval: 2000
        onTriggered: {
            pop_up_close_door.message = "Opération annulée avec succès!"
            timer.restart(timer.triggered(main_window.backClicked()))
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;formeditorZoom:0.6600000262260437;height:480;width:640}
}
##^##*/

