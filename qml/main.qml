import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    id: main_window
    width: Styles.width
    height: Styles.height
    visible: true
    color: Styles.backgroundColor
    flags: Qt.FramelessWindowHint | Qt.Window
    //uncoment the line above when Full screen is needed
    //visibility: Window.FullScreen

    signal backClicked
    signal showKeyboardView
    signal onOpenSettingsView
    signal onOpenProfileManagementView
    signal onOpenUserManagementView(int userType)
    signal showQrCodeView
    signal backHomeClicked
    signal onOpenLockerView
    signal onOpenDeliveryLayout

    property QtObject backend

    StackView {
        id: main_stack_view
        initialItem: main_component
        anchors.fill: parent
        focusPolicy: Qt.TabFocus
        onCurrentItemChanged: {
            currentItem.forceActiveFocus()
        }
    }

    Component {
        id: main_component
        AuthView {}
    }

    Component.onCompleted: {
        main_window.backClicked.connect(popBack)
        main_window.backHomeClicked.connect(backHome)
        main_window.showKeyboardView.connect(openKeyboardView)
        main_window.showQrCodeView.connect(openQrCodeView)
        main_window.onOpenSettingsView.connect(openSettingsView)
        main_window.onOpenProfileManagementView.connect(
                    openProfileManagementView)
        main_window.onOpenUserManagementView.connect(openUserManagementView)
        main_window.onOpenLockerView.connect(openLockerView)
        main_window.onOpenDeliveryLayout.connect(openDeliveryLayout)
    }

    function popBack() {
        main_stack_view.pop(StackView.Immediate)
    }

    function backHome() {
        main_stack_view.pop(null, StackView.Immediate)
    }

    function openKeyboardView() {
        main_stack_view.push(Qt.resolvedUrl("qrc:/qml/KeyboardView.qml"),
                             StackView.Immediate)
    }

    function openQrCodeView() {
        main_stack_view.push(Qt.resolvedUrl("qrc:/qml/QrCodeView.qml"),
                             StackView.Immediate)
    }

    function openSettingsView() {
        main_stack_view.push(Qt.resolvedUrl("qrc:/qml/SettingsView.qml"),
                             StackView.Immediate)
    }

    function openProfileManagementView() {
        main_stack_view.push(Qt.resolvedUrl(
                                 "qrc:/qml/ProfileManagementView.qml"),
                             StackView.Immediate)
    }

    function openUserManagementView(userType) {
        main_stack_view.push(Qt.resolvedUrl("qrc:/qml/UserManagementView.qml"),
                             {
                                 "userType": userType
                             }, StackView.Immediate)
    }

    function openLockerView() {
        main_stack_view.push(Qt.resolvedUrl("qrc:/qml/LockerView.qml"),
                             StackView.Immediate)
    }

    function openDeliveryLayout() {
        main_stack_view.push(Qt.resolvedUrl("qrc:/qml/DeliveryLayout.qml"),
                             StackView.Immediate)
    }
}
