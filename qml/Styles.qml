//Style.qml with custom singleton type definition
pragma Singleton

import QtQuick 2.0

QtObject {
    property color backgroundColor: "#6ab688"
    property color bannerBackgroundColor: "#031719"
    property color darkGreen: "#2C7873"
    property color lightGreen: "#3E9F99"
    property color deepCyan: "#004445"
    property color openDoor: "#00C008"
    //Door color--------
    property color selectDoor: "#00D1FF"
    property color fullDoor: "#CF2222"
    property color notTakenDoor: "#FFA235"
    property color retuorn: "#FFA235"
    //----------
    property int width: 1280
    property int height: 800
    property int topbarHeight: 90
    property int bannerHeight: 150
    property int defaultMargin: 30
    property FontLoader principalFont: FontLoader {
        source: "qrc:/fonts/OpenSans-Bold.ttf"
    }
    property FontLoader codeFont: FontLoader {
        source: "qrc:/fonts/RobotoMono-Bold.ttf"
    }
}
