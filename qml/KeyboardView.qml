import QtQuick 2.15
import QtQuick.Controls 2.15

Page {
    id: keyboard_view_page

    Rectangle {
        color: Styles.backgroundColor
        anchors.fill: parent
    }

    TopBar {
        id: keyboard_view_topbar
        height: Styles.topbarHeight
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 30
        anchors.topMargin: 30
        anchors.rightMargin: 30
        title: "Veuillez introduire votre code"

        BackButton {
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.rightMargin: 15
        }
    }

    KeyboardContent {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: keyboard_view_topbar.bottom
    }
}
