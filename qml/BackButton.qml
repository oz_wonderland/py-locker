import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11

RoundButton {
    id: back_button
    text: qsTr("Retour")
    font.weight: Font.Bold
    font.pointSize: 24
    onClicked: onClick()
    property var onClick: function () { main_window.backClicked() };

    Image {
        antialiasing: true
        id: image_back_button
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.alignWhenCentered: Text
        source: "/drawables/arrow_return_left.svg"
        anchors.leftMargin: 7
        anchors.bottomMargin: 10
        anchors.topMargin: 10
        fillMode: Image.PreserveAspectFit
    }

    contentItem: Text {
        font: Styles.principalFont.name
        color: "white"
        text: "Retour"
        anchors.rightMargin: 8
        anchors.leftMargin: 5
        elide: Text.ElideRight
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: image_back_button.right
        anchors.right: parent.right
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    background: Rectangle {
        implicitWidth: 145
        implicitHeight: 63
        opacity: enabled ? 1 : 0.3
        color: back_button.down ? Styles.lightGreen : Styles.deepCyan
        border.width: 0
        radius: 10
    }
}
