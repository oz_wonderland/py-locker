import QtQuick 2.15
import QtQuick.Controls 2.15

Page {
    id: settings_view_page

    //Background
    Rectangle {
        color: Styles.backgroundColor
        anchors.fill: parent
    }

    TopBar {
        id: settings_view_banner
        withIcon: true
        iconCirclePath: "/drawables/2_gears.svg"
        height: Styles.topbarHeight
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 30
        anchors.topMargin: 30
        anchors.rightMargin: 30
        title: "Paramètres"

        BackButton {
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.rightMargin: 15
            onClick: function() { main_window.backHome() }
        }
    }
    SettingsContent {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: settings_view_banner.bottom
    }
}
