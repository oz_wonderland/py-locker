import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11

Rectangle {
    id: locker_content_rectangle
    color: Styles.backgroundColor

    SubBar {
        id: locker_content_sub_bar
        barText: "Que souhaitez-vous faire?"
        anchors.top: parent.top
        anchors.topMargin: 10
    }

    RowLayout {
        anchors.top: locker_content_sub_bar.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: Styles.defaultMargin

        Layout.fillHeight: true
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

        Locker {
            id: locker2x4
            enabled: false
            numberOfColums: 2 //number of columns on the locker
        }

        ColumnLayout {
            Rectangle {
                color: Styles.deepCyan
            }

            spacing: Styles.defaultMargin

            IconButton {
                width_button: 250
                height_button: 100
                id: delivery_orders_button
                buttonText: "Préparer livraison"
                onClicked: main_window.onOpenDeliveryLayout()
            }

            IconButton {
                id: retour_orders_button
                width_button: 250
                height_button: 100
                buttonText: "Préparer retour"
                //TODO onClicked: Open Return Layout
            }

            IconButton {
                id: information_button
                width_button: 250
                height_button: 100
                buttonText: "Information"
                //TODO onClicked: - to do : Open Information Layout
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;formeditorZoom:0.5;height:800;width:1280}
}
##^##*/

