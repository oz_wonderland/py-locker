import QtQuick 2.15
import QtQuick.Layouts 1.11
import QtQuick.Controls 1.4
import QtQuick.VirtualKeyboard 2.15

Rectangle {
    id: keyboard_content_rectangle
    color: Styles.backgroundColor
    signal onKeyPressed(int index, string key)
    property var code: []

    RowLayout {
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.bottom: keyboard_content_input_panel.top
        spacing: 5

        Repeater {
            id: code_input_repeater
            model: 6
            CodeInput {
                itemNumber: index
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            }
        }
    }

    InputPanel {
        id: keyboard_content_input_panel
        y: parent.height - keyboard_content_input_panel.height
        width: Styles.width
        anchors.left: parent.left
        anchors.right: parent.right
        active: false
    }

    Component.onCompleted: {
        resetRepeater(code_input_repeater)
        keyboard_content_input_panel.visible = true
        keyboard_content_rectangle.onKeyPressed.connect(keyPressed)
    }

    PopUpIcon {
        id: popup_error_message
        message: qsTr("code invalide")
        onClosed: {
            resetRepeater(code_input_repeater)
            keyboard_content_input_panel.visible = true
            keyboard_content_rectangle.onKeyPressed.connect(keyPressed)
        }
    }

    function keyPressed(index, key) {
        code[index] = key
        if (code.length !== 6) {
            return
        }
        keyboard_content_input_panel.visible = false
        codeCheckerConnector.codeverify(code.join(''))
    }

    function resetRepeater(repeater) {
        code = []
        repeater.itemAt(0).focus = true
        repeater.itemAt(0).reset()
        repeater.itemAt(1).reset()
        repeater.itemAt(2).reset()
        repeater.itemAt(3).reset()
        repeater.itemAt(4).reset()
        repeater.itemAt(5).reset()
    }

    Connections {
        target: backend

        function onOpenSettingsView() {
            main_window.onOpenSettingsView()
        }
        function onOpenErrorPopup() {
            popup_error_message.open()
        }
    }
}
