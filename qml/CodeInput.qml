import QtQuick 2.15
import QtQuick.Controls 2.15

FocusScope {
    width: code_input_rectangle.width
    height: code_input_rectangle.height
    x: code_input_rectangle.x
    y: code_input_rectangle.y
    property int itemNumber

    Item {
        Rectangle {
            id: code_input_rectangle
            color: "white"
            width: 100
            height: 100
            radius: 10

            TextField {
                id: code_input_text_field
                inputMask: "N"
                opacity: 1
                anchors.fill: parent
                focus: true
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                placeholderText: qsTr("*")
                leftPadding: 0
                anchors.margins: 5
                font.pointSize: 55
                maximumLength: 1
                font.family: Styles.codeFont.name
                font.underline: false
                font.bold: false
                font.hintingPreference: Font.PreferDefaultHinting
                font.capitalization: Font.AllUppercase
                cursorVisible: false
                onTextChanged: {
                    if (!acceptableInput) {
                        return
                    }
                    nextItemInFocusChain().forceActiveFocus()
                    keyboard_content_rectangle.onKeyPressed(
                                itemNumber, code_input_text_field.text)
                }
                Keys.onPressed: {
                    if (event.key == Qt.Key_Backspace && itemNumber != 0) {
                        nextItemInFocusChain(false).forceActiveFocus()
                    }
                }
            }
        }
    }

    function reset() {
        code_input_text_field.text = ""
        code_input_text_field.cursorPosition = 0
    }
}
