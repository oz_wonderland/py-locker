import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.11
import QtMultimedia 5.15
import QrCodeReader 1.0

Rectangle {
    id: qr_content_rectangle
    color: Styles.backgroundColor

    Camera {
        id: camera
    }

    QrCodeReader {
        id: qr_filter
    }

    RowLayout {
        id: qr_content_row_layout
        anchors.fill: parent
        spacing: 10

        AnimatedImage {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            source: "qrc:/drawables/arrow_rigth.gif"
            width: Styles.width / 2
            Layout.margins: 40
        }
        VideoOutput {
            Layout.fillHeight: true
            Layout.fillWidth: true
            id: qr_video_output
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            source: camera
            autoOrientation: true
            width: Styles.width / 2
            Layout.margins: 40
            filters: [qr_filter]
        }
    }
}
