import QtQuick 2.15
import QtQuick.Controls 2.15
import "."

Page {
    id: user_view_page

    property string userType

    // background -----------------
    Rectangle {
        anchors.fill: parent
        color: Styles.backgroundColor
    }

    //-----------------------------
    Locker {
        id: locker2x4
        enabled: false
        numberOfColums: 2
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.verticalCenterOffset: 0
        anchors.leftMargin: 21
    }

    TopBar {
        id: top_bar
        iconCirclePath: "/drawables/info_icon.svg"
        anchors.left: locker2x4.right
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.leftMargin: 32
        anchors.rightMargin: 23
        anchors.topMargin: 42
    }

    Rectangle {
        id: rectangle_message
        anchors.right: parent.right
        anchors.top: top_bar.bottom
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 32
        anchors.leftMargin: 762
        anchors.topMargin: 50
        anchors.rightMargin: 23
        color: Styles.white
        opacity: 0.8
        radius: 10
        anchors.left: parent.left

        Text {
            font.family: "Arial"
            font.pixelSize: 24
            font.underline: false
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Que souhaitez-vous faire?"
            anchors.verticalCenter: parent.verticalCenter
            font.bold: true
        }
    }

    Column {
        id: row_of_buttons
        anchors.right: rectangle_message.left
        anchors.top: top_bar.bottom
        anchors.topMargin: 50
        anchors.rightMargin: 35
        spacing: 25

        IconButton {
            id: delivery_orders_button
            width_button: 170
            height_button: 100
            buttonText: "Préparer\nLivraison"
            onClicked: main_window.onOpenDeliveryLayout()
        }

        IconButton {
            id: retour_orders_button
            width_button: 170
            height_button: 100
            buttonText: "Préparer\nRetour"
        }

        IconButton {
            id: settings_button
            width_button: 170
            height_button: 100
            iconSource: "/drawables/Gears.png"
        }

        IconButton {
            id: exit_button
            width_button: 170
            height_button: 70
            iconSource: "/drawables/box_arrow_left.svg"
            buttonText: "Sortir"
            onClicked: main_window.backHomeClicked()
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:4}
}
##^##*/

