import QtQuick 2.15
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.11

Rectangle {
    id: user_management_content_rectangle
    property int userType
    color: Styles.backgroundColor

    SubBar {
        id: user_management_sub_bar
        barText: userType === 1 ? "ADMIN" : "ASV"
        anchors.top: parent.top
        anchors.topMargin: 10
    }
    ListView {
        id: user_management_list_view
        anchors.verticalCenter: parent.verticalCenter
        anchors.top: user_management_sub_bar.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.rightMargin: 30
        anchors.leftMargin: 30
        anchors.topMargin: 38
        spacing: 10
        model: userConnector
        orientation: ListView.Horizontal
        delegate: user_management_delegate_component

        Component {
            id: user_management_delegate_component
            RoundButton {
                id: profile_button
                Text {
                    id: profile_button_firstname
                    text: firstname
                    font.weight: Font.Bold
                    font.pointSize: 28
                    color: "white"
                    anchors.top: parent.top
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                Text {
                    id: profile_button_lastname
                    text: lastname
                    font.weight: Font.Bold
                    font.pointSize: 28
                    color: "white"
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                Text {
                    id: profile_button_type
                    text: type
                    font.weight: Font.Bold
                    font.pointSize: 28
                    color: "white"
                    anchors.bottom: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                background: Rectangle {
                    id: background
                    implicitWidth: 284
                    implicitHeight: 234
                    opacity: enabled ? 1 : 0.3
                    color: profile_button.down ? Styles.lightGreen : Styles.deepCyan
                    border.width: 0
                    radius: 10
                }
            }
        }
    }
    //Icon Buttons bellow the subbar
    //New admin
    IconButton {
        id: user_management_new_button
        anchors.bottom: parent.bottom
        anchors.margins: 30
        anchors.horizontalCenter: parent.horizontalCenter
        width_button: 448
        height_button: 56
        iconSource: "/drawables/person_plus_fill.svg"
        buttonText: "Nouveau"
    }

    Component.onCompleted: userConnector.getUsersByType(userType)
}
