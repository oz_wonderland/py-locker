import QtQuick 2.15
import QtQuick.Controls 2.15

Popup {
    property string message
    property string button_text_left_popup
    property string button_text_rigth_popup

    function onClickedYes() {}

    id: popup
    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
    anchors.centerIn: Overlay.overlay
    opacity: 1
    Rectangle {
        id: popup_rectangle_message
        width: 1162
        height: 227
        radius: 10
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        color: "#3E9F99"
        Text {
            id: popup_rectangle_message_text
            text: message
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.Wrap
            font.weight: Font.Bold
            font.family: Styles.principalFont.name
            font.pointSize: 48
            color: "white"
        }
    }

    Row {
        anchors.top: popup_rectangle_message.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 44
        spacing: 30

        RoundButton {
            id: button_left_popup
            font.weight: Font.Bold
            font.pointSize: 48
            font.family: Styles.principalFont.name
            onClicked: popup.close()

            contentItem: Text {
                font: button_left_popup.font
                color: "white"
                text: button_text_left_popup
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

            background: Rectangle {
                implicitWidth: 510
                implicitHeight: 98
                opacity: enabled ? 1 : 0.3
                color: button_left_popup.down ? "#D86363" : "#FF5F5F"
                border.width: 0
                radius: 10
            }
        }

        RoundButton {
            id: button_rigth_popup
            font.weight: Font.Black
            font.pointSize: 48
            font.family: Styles.principalFont.name
            onClicked: {
                onClickedYes()
            }

            contentItem: Text {
                font: button_rigth_popup.font
                text: button_text_rigth_popup
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                color: "white"
            }

            background: Rectangle {
                implicitWidth: 510
                implicitHeight: 98
                opacity: enabled ? 1 : 0.3
                color: button_rigth_popup.down ? "#2C7873" : "#3E9F99"
                border.width: 0
                radius: 10
            }
        }
    }
}
