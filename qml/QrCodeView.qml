import QtQuick 2.15
import QtQuick.Controls 2.15

Page {
    id: settings_view_page

    Rectangle {
        color: Styles.backgroundColor
        anchors.fill: parent
    }

    TopBar {
        id: settings_view_topbar
        height: Styles.topbarHeight
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 30
        anchors.topMargin: 30
        anchors.rightMargin: 30
        title: "Scannez votre QR code"

        BackButton {
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.rightMargin: 15
        }
    }

    QrCodeContent {
        id: qr_code_content
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: settings_view_topbar.bottom
    }
}
