import QtQuick 2.15
import QtQuick.Controls 2.15

Button {
    property string title
    property string imageSource
    id: auth_method_button

    Image {
        id: auth_method_image
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        source: imageSource
        anchors.horizontalCenter: parent.horizontalCenter
        fillMode: Image.PreserveAspectFit
    }

    contentItem: Text {
        id: auth_method_text
        color: "white"
        text: qsTr(title)
        anchors.top: auth_method_image.bottom
        font.pixelSize: 50
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.Wrap
        anchors.horizontalCenter: parent.horizontalCenter
        font.bold: true
        anchors.topMargin: 0
    }

    background: Rectangle {
        id: auth_method_background
        color: auth_method_button.down ? Styles.darkGreen : Styles.backgroundColor
        implicitWidth: 570
        implicitHeight: 570
    }
}
