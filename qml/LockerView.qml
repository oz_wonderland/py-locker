import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11

Page {
    id: locker_view_page

    Rectangle {
        color: Styles.backgroundColor
        anchors.fill: parent
    }

    TopBar {
        id: locker_view_topbar

        height: Styles.topbarHeight
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: Styles.defaultMargin
        anchors.topMargin: Styles.defaultMargin
        anchors.rightMargin: Styles.defaultMargin
        iconCirclePath: "/drawables/info_icon.svg"
        title: "Paramètres du casier"

        BackButton {
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.rightMargin: 15
        }
    }

    LockerContent {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: locker_view_topbar.bottom
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:800;width:1280}
}
##^##*/

