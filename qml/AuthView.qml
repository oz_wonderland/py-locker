import QtQuick 2.15
import QtQuick.Controls 2.15

Page {
    id: auth_view_page

    Rectangle {
        color: Styles.backgroundColor
        anchors.fill: parent
    }

    TopBar {
        id: auth_view_topbar
        height: Styles.topbarHeight
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 30
        anchors.topMargin: 30
        anchors.rightMargin: 30
        title: "Choisissez le mode"
    }

    AuthContent {
        id: auth
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: auth_view_topbar.bottom
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/

