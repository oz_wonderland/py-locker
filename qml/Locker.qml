import QtQuick 2.12
import Qt.labs.qmlmodels 1.0
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.15
import QtQuick.Controls.Styles 1.4
import "js/Locker.js" as Locker

Item {
    property int numberOfColums
    property int currentIndex: -1
    property int lastIndex: -1

    signal open

    //property int openDoorIndex
    id: locker_item
    width: 500
    height: 564

    onCurrentIndexChanged: {
        console.log("Current Index: " + currentIndex)
        switch (lastIndex) {
        case -1:
            lastIndex = currentIndex
            break
        default:
            locker_repeater.itemAt(lastIndex).stateButton = 'empty'
            lastIndex = currentIndex
            break
        }
    }

    Grid {
        id: locker_layout
        width: 125 * numberOfColums
        height: 30
        anchors.horizontalCenter: parent.horizontalCenter
        columns: numberOfColums

        Repeater {
            id: locker_repeater
            model: numberOfColums * 4

            delegate: Item {
                id: door
                property string stateButton: "empty"

                width: 125
                height: Locker.det(numberOfColums)

                Button {
                    property color bgColor

                    onClicked: {
                        locker_repeater.itemAt(index).stateButton
                                === 'selected' ? locker_repeater.itemAt(
                                                     index).stateButton
                                                 = 'empty' : locker_repeater.itemAt(
                                                     index).stateButton = 'selected'
                        currentIndex = index
                    }

                    id: container
                    text: index
                    anchors.fill: parent
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.bold: true
                    font.pointSize: 36
                    enabled: true
                    state: stateButton
                    // state: {
                    //     if (openDoorIndex === index) {
                    //         return "open"
                    //     } else {
                    //         return stateButton
                    //     }
                    // }
                    background: Rectangle {
                        border.width: .5
                        border.color: "#000000"
                        color: container.bgColor
                    }
                    states: [
                        State {
                            name: "empty"
                            PropertyChanges {
                                target: container
                                bgColor: "white"
                            }
                        },
                        State {
                            name: "selected"
                            PropertyChanges {
                                target: container
                                bgColor: Styles.selectDoor
                            }
                        },
                        State {
                            name: "open"
                            PropertyChanges {
                                target: container
                                bgColor: Styles.openDoor
                            }
                        },
                        State {
                            name: "full"
                            PropertyChanges {
                                target: container
                                bgColor: Styles.fullDoor
                            }
                        },
                        State {
                            name: "notTaken"
                            PropertyChanges {
                                target: container
                                bgColor: Styles.notTakenDoor
                            }
                        },
                        State {
                            name: "return"
                            PropertyChanges {
                                target: container
                                bgColor: Styles.retuorn
                            }
                        }
                    ]
                }

                InnerShadow {
                    id: rectShadow
                    anchors.fill: source
                    cached: true
                    horizontalOffset: -3
                    verticalOffset: -3
                    radius: 10.0
                    samples: 16
                    color: "#80000000"
                    smooth: true
                    source: container
                }
            }
        }
    }
}
