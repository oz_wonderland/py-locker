resourcesDir = resources
$(eval SYSTEM=$(shell sh -c "uname -s"))

install:
	@echo "Installing requirements"
	@pip3 install -r requirements.txt

zbar:
	@echo "Installing zbar"
	@echo "Running on $(SYSTEM)"
	@$(MAKE) $(SYSTEM)

Darwin:
	brew install zbar

Linux:
	@echo "run sudo apt-get install -y libzbar0"	

build: | $(resourcesDir)
	pyside2-rcc -o resources.py resources.qrc

$(resourcesDir):
	@echo "Folder $(resourcesDir) does not exist"
	mkdir -p $@

run: build
	python3 main.py

update_requirements:
	pip3 freeze > requirements.txt

format_qml:
	qmlfmt -w ./qml/