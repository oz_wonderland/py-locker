<p align="center">
  <a href="" rel="noopener">
</p>

<h3 align="center">py-locker</h3>

---

<p align="center"> py-locker is an app for a connected locker.<br>
</p>

## 📝 Table of Contents
- [About](#about)
- [Getting Started](#getting_started)
- [Deployment](#deployment)
- [Usage](#usage)
- [Built Using](#built_using)
- [TODO](../TODO.md)
- [Contributing](../CONTRIBUTING.md)
- [Authors](#authors)
- [Acknowledgments](#acknowledgement)

## 🧐 About <a name = "about"></a>
Py-locker can be runned by itself on a standalone manner. But it can also be used with a backend to be smarter
## 🏁 Getting Started <a name = "getting_started"></a>
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### Prerequisites
You need to install
 - [python3.8](https://www.python.org/).

### Installing
Once all the prerequisites are installed do the following
```
$> make zbar 
$> make install
$> make run
```

It should automagically open a window with the app running.

You are done.

## 🔧 Running the tests <a name = "tests"></a>
WIP

### Break down into end to end tests
WIP
### And coding style tests
WIP

## 🎈 Usage <a name="usage"></a>
WIP

## 🚀 Deployment <a name = "deployment"></a>
WIP

## ⛏️ Built Using <a name = "built_using"></a>
- [python3](https://www.python.org/)

## ✍️ Authors <a name = "authors"></a>
- [@oz_wonderland](https://gitlab.com/oz_wonderland) - Initial work & management
- [@edu_paulos](https://gitlab.com/edu_paulos) - Development, prototyping & management
- [@fransimao](https://gitlab.com/fransimao) - Development
- [@HCarrinho](https://gitlab.com/HCarrinho) - Development

See also the list of [contributors](https://gitlab.com/oz_wonderland/zero/contributors) who participated in this project.

## 🎉 Acknowledgements <a name = "acknowledgement"></a>
- Hat tip to anyone whose code was used
- Inspiration
- References
